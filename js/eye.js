var jseyeso = null, jseye = null;

var browserversion = 0.0;
var browsertype = 0;

function browserdetect() {
    var agt = navigator.userAgent.toLowerCase();
    var appVer = navigator.appVersion.toLowerCase();
    browserversion = parseFloat(appVer);
    var iePos = appVer.indexOf('msie');
    if (iePos != -1) browserversion = parseFloat(appVer.substring(iePos + 5, appVer.indexOf(';',iePos)));
    var nav6Pos = agt.indexOf('netscape6');
    if (nav6Pos != -1) browserversion = parseFloat(agt.substring(nav6Pos + 10));
    browsertype = (iePos != -1) ? 1 : (agt.indexOf('mozilla') != -1) ? 2 : 0;
    return(browsertype > 0);
}

browserdetect();

function jseyesmove(x, y) {
    // console.log("мышка: " + x, y);
    var ex, ey, dx, dy;
    if (jseyeso && jseye) {
        ex = $("#jseyeslayer").offset().left + 50;
        ey = $("#jseyeslayer").offset().top + 50;
        // console.log("зрачек: " + ex, ey);
        dx = x - ex; dy = y - ey;
        // r = (dx * dx / 1000 + dy * dy / 1000 > 1) ? Math.sqrt(1000 * 1000 / (dx * dx * 1000 + dy * dy * 1000)) : 1;
        var len =  Math.sqrt((ey - y)*(ey - y) + (ex - x)*(ex - x));
        // console.log(len);
        r = Math.sqrt(1000 * 1000 / (dx * dx * 1000 + dy * dy * 1000)) * (len / 800);
        jseye.style.left = r * dx + 40 + "px"; jseye.style.top = r * dy + 40 + "px";
        // console.log("стиль зрачка: " + jseye.style.left, jseye.style.top)
        // ex += 56; dx -= 56;
        // r = (dx * dx / 100 + dy * dy / 100 < 1) ? 1 : Math.sqrt(100 * 100 / (dx * dx * 100 + dy * dy * 100));
    }
}

function jseyes() {
    if (browsertype > 0 && browserversion >= 5) {
        jseyeso = $('#jseyeslayer')[0];
        jseye = $('#jseye')[0];
        switch (browsertype) {
            case 1:
            document.onmousemove = jseyesmousemoveIE;
            break;
      
            case 2:
            document.captureEvents(Event.MOUSEMOVE);
            document.onmousemove = jseyesmousemoveNS;
            break;
        }
    }
}

function jseyesmousemoveNS(e) {
    jseyesmove(e.pageX, e.pageY);
}

function jseyesmousemoveIE() {
    jseyesmove(event.clientX + document.documentElement.scrollLeft, event.clientY + document.documentElement.scrollTop);
}

$(document).ready(function() {
    jseyes();
});