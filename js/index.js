var books;

/*--------------------------------------------------------------------------------------------------------------------*/
/* Books */
/*--------------------------------------------------------------------------------------------------------------------*/
_books = function() {
    var t = this;
    t.name = 'books';
    shield.show();
    $.get('https://netology-fbb-store-api.herokuapp.com/book/', function(data) {
        console.log(data);
        t.init(data);
        shield.hide();
    });
};

_books.prototype = {
    init: function(data) {
        var t = this, i, IdList;
        t.data = data;
        t.itemsElement = $('.books__items');
        t.items = [];
        IdList = [];

        $(t.data).each(function(i) {
            t.items.push(new _book(t, t.data[i]));
        });

        t.additiveButton = $('.books__additive__button');
        t.additiveButton.on("click", function(){
            t.show();
            IdList = [];
            $(".books__item").each(function(i, item) {
                IdList.push($(item).attr("id"));
            });
            $.cookie("sort-state", IdList);
            t.start = IdList.length;
        });

        t.start = 0;

        if (!$.cookie('sort-state') || $.cookie('sort-state') === "null") {
            t.show();
        } else {
            t.showWithCookie();
        };

        t.findInput = $('.search__field');
        t.findInput.val("");
        t.findInput.keyup(function(e){t.findBooks(e)});

        var buttonAdditiveState = $.cookie("button-additive-state");
        $(".books__additive__button").css("display", buttonAdditiveState);

        $(".about__link").on("click", function() {
            $.cookie('button-additive-state', null);
            $.cookie('sort-state', null);
            t.start = 0;
        });
    },

    show: function(e) {
        var t = this, i, end = t.start + 4;
        for (i = t.start; i < end; i++) {
            if (t.items[i]) {
                t.items[i].show();
            }
            else {
                t.additiveButton.hide();

                var buttonAdditiveDisplay = $(".books__additive__button").css("display");
                $.cookie("button-additive-state", buttonAdditiveDisplay);
                break;
            };
        };
        t.start = end;
    },

    showWithCookie: function() {
        var t = this, i, end = t.start + 4;
        var sortState = $.cookie('sort-state');
        var IdList = sortState.split(',');

        $.each(IdList, function(i, itemArr) {
            $.each(t.items, function(i, item) {
                if (item.data.id === itemArr) {
                    item.show();
                };
            });
        });
        t.start = IdList.length;
    },

    findBooks: function(e) {
        $.cookie('button-additive-state', null);
        $.cookie('sort-state', null);
        var t = this;
        var value = e.target.parentElement.children[0].value;

        if (value === "") {
            $.each(t.items, function(i, item) {
                item.hide();
            });
            for (i = 0; i < 4; i++) {
                t.items[i].show();
            };
            t.additiveButton.show();
            t.start = 4;
            return false;
        };

        $.each(t.items, function(i, item) {
            item.hide();
            t.additiveButton.hide();
            var mainStr = item.data.title + " " + item.data.description + " " + item.data.info;
            var mainStrResult = mainStr.replace(/<\/?[^>]+>/g,'');
            var regexp = new RegExp(value, "gi");
            var indexSearch = mainStrResult.search(regexp);

            if (!(indexSearch === -1)) {
                item.show();
            };
        });
    },
};

/*--------------------------------------------------------------------------------------------------------------------*/
/* Book */
/*--------------------------------------------------------------------------------------------------------------------*/
_book = function(parent, data) {
    var t = this;
    t.name = 'book';
    t.parent = parent;
    t.data = data;
    t.element = templates['Book'].element.clone();
    t.setData();
};

_book.prototype = {
    setData: function() {
        var t = this, href;

        t.element.attr("id", t.data.id);
        t.element.find('[data-param="title"]').text(t.data.title);
        t.element.find('[data-param="info"]').html(t.data.info);
        href = t.element.find('[data-param="link"]').attr('href') + t.data.id;
        t.element.find('[data-param="link"]').attr('href', href);
        t.element.find('[data-param="cover"]').attr('src', t.data['cover']['small']);
    },

    show: function() {
        var t = this;
        t.element.appendTo(t.parent.itemsElement);
    },

    hide: function() {
        var t = this;
        t.element.detach();
    }
};

$(document).ready(function() {
    books = new _books();
    console.log(books);

    // Сортировка
    $(function() {
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
    });
    
    // Cookies 
    $(function() {
        $.fn.mysort = function(){
            this.sortable({
                update: function() {
                    var ser = $(this).sortable('toArray');
                    $.cookie('sort-state', ser);
                }
            }).show();
        };
        
        if ($.cookie('sort-state')){
            var pos = $.cookie('sort-state').split(',');
            $(pos).each(function(i){
                $("#sortable").append($('ul#sortable li#'+pos[i]+'')).mysort();
            });
        } else {
            $("#sortable").mysort();
        };
    });

    isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    if (isMobile.any()) {
        $(".books__pre").hide();
    };
});
