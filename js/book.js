var book;

/*--------------------------------------------------------------------------------------------------------------------*/
/* Book */
/*--------------------------------------------------------------------------------------------------------------------*/
_book = function() {
    var t = this;
    t.name = 'book';
    t.element = $('.book');
    shield.show();
    $("#jseyeslayer").hide();
    t.id = document.location.search.replace(/[?id=]/gi, "");
    if (!t.id) document.location.href = '/';

    $.get("https://netology-fbb-store-api.herokuapp.com/book/" + t.id, function(data) {
        t.init(data);
        $.get("http://university.netology.ru/api/currency/", function(data) {
			t.initCurrency(data);
            shield.hide();
		});
    });
};

_book.prototype = {
    init: function(data) {
        var t = this, i;
        t.data = data;
        t.reviewsElement = $('.book__reviews');
        t.reviewsItems = [];

        $(t.data.reviews).each(function(i) {
            t.reviewsItems.push(new _review(t, t.data.reviews[i]));
        });

        t.featuresElement = $('.book__features');
        t.featuresItems = [];

        $(t.data.features).each(function(i) {
            t.featuresItems.push(new _feature(t, t.data.features[i]));
        });
    },

    initCurrency: function(data) {
    	var t  = this, i;
    	t.currencyData = data;

        $(t.currencyData).each(function(i) {
            if (t.data.currency == t.currencyData[i].ID) {
                t.data.currencyCharCode = t.currencyData[i].CharCode;
                return false;
            }
        });
        
        t.setData();
    },

    setData: function() {
        var t = this;
        var href = t.element.find('[data-param="order-link"]').attr('href') + t.data.id;
        t.element.find('[data-param="title"]').text(t.data.title);
        t.element.find('[data-param="author-name"]').text(t.data.author.name);
        t.element.find('[data-param="author-pic"]').attr('src', t.data.author.pic);
        t.element.find('[data-param="cover-large"]').attr('src', t.data.cover.large);
        t.element.find('[data-param="description"]').html(t.data.description);
        t.element.find('[data-param="order-link"]').attr('href', href);
        t.element.find(".book__price").text(t.data.price);
        t.element.find(".book__price-charcode").text(t.data.currencyCharCode);
        $("#jseyeslayer").show();
    }
};

_review = function(parent, data) {
    var t = this;
    t.name = 'review';
    t.parent = parent;
    t.data = data;
    t.element = templates['Reviews'].element.clone().appendTo(t.parent.reviewsElement);
    t.setData();
};

_review.prototype = {
    setData: function() {
        var t = this;
        t.element.find('[data-param="reviews-author-name"]').text(t.data.author.name);
        t.element.find('[data-param="reviews-author-pic"]').attr('src', t.data.author.pic);
        t.element.find('[data-param="reviews-cite"]').text(t.data.cite);
    }
};

_feature = function(parent, data) {
    var t = this;
    t.name = 'review';
    t.parent = parent;
    t.data = data;
    t.element = templates['Features'].element.clone().appendTo(t.parent.featuresElement);
    t.setData();
};

_feature.prototype = {
    setData: function() {
        var t = this;
        t.element.find('[data-param="features-pic"]').attr('src', t.data.pic);
        t.element.find('[data-param="features-title"]').text(t.data.title);
    }
};

$(document).ready(function() {
    book = new _book();
});