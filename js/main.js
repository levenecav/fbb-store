var shield, templates;

/*--------------------------------------------------------------------------------------------------------------------*/
/* Shield */
/*--------------------------------------------------------------------------------------------------------------------*/
_shield = function() {
    var t = this;
    t.element = $('<div class="c-shield "><div class="c-shield-bgr"></div></div>');
    t.loadingElement = $('<div class="c-shield-loading"/>').appendTo(t.element);
    t.loadingInnerElement = $('<div class="c-shield-loading-inner"/>').appendTo(t.loadingElement);
    t.element.appendTo($('body'));
    t.status = false;
};

_shield.prototype = {
    show: function() {
        var t = this;
        if (t.status) return false;
        t.status = true;
        t.spinner();
        t.element.show();
    },

    hide: function() {
        var t = this;
        t.status = false;
        t.element.hide();
    },

    spinner: function() {
        var t = this, top;
        if (!t.status) return false;
        top = parseInt(t.loadingInnerElement.css('top')) - 78;
        if (top < -858) top = 0;
        t.loadingInnerElement.css('top', top);
        setTimeout(function() {t.spinner();}, 80);
    }
};

/*--------------------------------------------------------------------------------------------------------------------*/
/* Templates */
/*--------------------------------------------------------------------------------------------------------------------*/
_templates = function() {
    var t = this;
    t.name = 'templates';
    t.element = $('.c-templates');
    t.element.find('.c-template').each(function(){
        t[$(this).data('id')] = new t._template($(this));
    });
    t.element.remove();
};

_templates.prototype._template = function(element) {
    var t = this;
    t.name = 'template';
    t.element = element.find('div').eq(0);
    t.element.detach();
};


$(document).ready(function() {
    shield = new _shield();
    templates = new _templates();
});
