module.exports = function(grunt) {

	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),
		less: {
			build: {
				files: {
					'css/main.css': 'styles/main.less'
				}
			}
		},
		ftp_push: {
		    demo: {
		    	options: {
		    		authKey: 'netology',
		    		host: 'university.netology.ru',
		    		dest: '/fbb-store/',
		    		port: 21
		    	},
		    	files: [{
		    		expand: true,
		    		cwd: '.',
		    		src: [
		    		      'index.html',
		    		      'book.html',
		    		      'order.html',
		    		      'about.html',
		    		      'css/main.css',
		    		      'js/*',
		    		      'css/*',
		    		      'img/*',
		    		      'img/persons/*'
		    		]
		        }]
		    }
		 }
	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-ftp-push');
	
	grunt.registerTask('default', ['less', 'ftp_push']);
	grunt.registerTask('start', ['less']);

};