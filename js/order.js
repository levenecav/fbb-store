/*--------------------------------------------------------------------------------------------------------------------*/
/* Order */
/*--------------------------------------------------------------------------------------------------------------------*/
_order = function() {
    var t = this;
    t.name = 'order';
    t.element = $('.order');
	t.formElement = t.element.find('form');
	t.submitButton = t.element.find('.btn_submit');
    shield.show();

    t.id = document.location.search.replace(/[?id=]/gi, "");
    console.log(t.id);

    if (!t.id) document.location.href = '/';

    $.get("https://netology-fbb-store-api.herokuapp.com/book/" + t.id, function(data) {
        t.init(data);
        $.get("http://university.netology.ru/api/currency/", function(data) {
			t.initCurrency(data);
			$.get("https://netology-fbb-store-api.herokuapp.com/order/delivery", function(data) {
				t.delivery = new _delivery(t, data);
				$.get("https://netology-fbb-store-api.herokuapp.com/order/payment", function(data) {
					t.payment = new _payment(t, data);
					shield.hide();
				});
			});
		});

		t.formElement.on("submit", function(e) {
			e.preventDefault();
			t.submit();
			return false;
		});
    });
};

_order.prototype = {
    init: function(data) {
        var t = this;
        t.data = data;
        t.setData();
        t.price = t.data.price;
        t.currencyId = t.data.currency;

        $('input[name="phone"]').on("change keyup input click", function(e) {
        	var value = this.value;
			var reg = /[^0-9\-]/gi;
			if (reg.test(value)) {
				value = value.replace(reg, '');
				this.value = value;
			};
		});
    },

    setData: function() {
        var t = this;
        t.element.find('[data-param="title"]').text(t.data.title);
        t.element.find('[data-param="cover-small"]').attr('src', t.data.cover.small);
        href = t.element.find('[data-param="link"]').attr('href') + t.id;
        t.element.find('[data-param="link"]').attr('href', href);
    },

    initCurrency: function(data) {
    	var t  = this, i;
    	t.currencyData = data;
    	
    	$.each(t.currencyData, function(i, item) {
	        $("select").append($("<option value='" + item.CharCode + "'>" + item.CharCode + "&nbsp;" + item.Name + "</option>"));
	    });

        $.each(t.currencyData, function(i, item) {
            if (t.currencyId == t.currencyData[i].ID) {
                t.charCode = t.currencyData[i].CharCode;
                return false;
            }
        });
    	bookPrice = t.price;
    	$(".order__price").text(bookPrice);
    	$(".order__price-charcode").text(t.charCode);
    },

    checkFields: function() {
    	var t = this;

    	$(".error-field").remove();

    	// NAME
    	if (!$('input[name="name"]').val()) {
    		$('input[name="name"]').after("<div class='error-field'><span>напишите имя</span></div>");
    	};

    	//  PHONE
    	if (!$('input[name="phone"]').val()) {
    		$('input[name="phone"]').after("<div class='error-field'><span>напишите телефон</span></div>");
    	};

    	// EMAIL
    	var email = $('input[name="email"]').val();
  	    if(email != 0) {
	    	if(!isValidEmail(email)) {
	    		$('input[name="email"]').after("<div class='error-field_email'><span>неккректный email</span></div>");
	    		t.correctEmail = false;
	    	} else {
	    		t.correctEmail = true;
	    	};
	    } else {
	    	$('input[name="email"]').after("<div class='error-field'><span>напишите email</span></div>");
	    };

		function isValidEmail(email) {
	    	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	    	return pattern.test(email);
	    };

	    // DELIVERY
	    if (!$('input[name="deliveryId"]:checked').length) {
	    	$('.delivery__items').after("<div class='error-field'><span>выберите доставку</span></div>");
	    };

	    // PAYMENT
	    if (!$('input[name="paymentId"]:checked').length) {
	    	$('.payment__items').after("<div class='error-field'><span>выберите способ оплаты</span></div>");
	    };

	    // ADDRESS
	    if (!$('textarea[name="deliveryAddress"]').val() && $(".delivery__address").css('display') === "block") {
    		$('textarea[name="deliveryAddress"]').after("<div class='error-field'><span>напишите адрес</span></div>");
    	};

	    // COMMENT
	    if (!$('textarea[name="comment"]').val()) {
    		$('textarea[name="comment"]').after("<div class='error-field'><span>напишите тут что-нибудь</span></div>");
    	};
    },

	submit: function() {
		var t = this, url, book, paymentCurrency;
		url = t.formElement.attr("action");
		book = t.id;
		paymentCurrency = t.currencyId;

		$('input[name="book"]').val(book);
		$('input[name="manager"]').val('levenecav@yandex.ru');
		$('input[name="paymentCurrency"]').val(paymentCurrency);
		t.checkFields();

		if ($(".error-field").length) {
    		$(".error").remove();
			t.submitButton.after("<div class='error-field'><span>Не все данные введены</span></div>");
			return false;
    	} else if (!t.correctEmail) {
    		$(".error").remove();
			t.submitButton.after("<div class='error-field'><span>Неккоректно введен email</span></div>");
			return false;
    	};

		var data = t.formElement.serialize();
		shield.show();

		$.ajax({
			type: 'POST',
			url: url,
			data: data,
			success: function success(data) {
				shield.hide();
				$(".currency").hide();
				t.formElement.replaceWith("<div class='order__success'><p>Заказ на книгу успешно оформлен.</p><p>Спасибо, что спасли книгу от сжигания в печи.</p></div>");
			},
			error: function(data) {
				shield.hide();
				if (data.status === 500) {
					console.log(data.status);
					$(".error").remove();
					t.submitButton.after("<div class='order__error'><p>Ошибка сервера.</p></div>");
				};
				
			}
		});
	}
};

/*--------------------------------------------------------------------------------------------------------------------*/
/* Delivery */
/*--------------------------------------------------------------------------------------------------------------------*/

_delivery = function(parent, data) {
    var t = this, i, ii;
	t.parent = parent;
	t.data = data;
    t.name = 'delivery';
    t.element = $('.delivery__items');
	t.deliveryAddressElement = t.parent.element.find('.delivery__address');

	$(t.data).each(function(i) {
        $(t.parent.currencyData).each(function(ii) {
            if (t.parent.currencyData[ii].ID == data[i].currency) {
                t.data[i].currencyCharCode = t.parent.currencyData[ii].CharCode;
                t.data[i].currencyNominal = t.parent.currencyData[ii].Nominal;
                t.data[i].currencyValue = t.parent.currencyData[ii].Value;
                return false;
            }
        });
		$("<div class='delivery__item'><input class='delivery__item__input' type='radio' name='deliveryId' id='" + t.data[i].id + "' value='" + t.data[i].id + "'/><label for=" + t.data[i].id + "> " + t.data[i].name + " — <span class='delivery__price'>" + t.data[i].price + "</span> <span class='delivery__charcode'>" + t.data[i].currencyCharCode + "</span></label></div>").appendTo($(t.element));
    });

	$(".delivery__item__input").on("change", function(){
		t.showValues($(this).attr('id'));
	});

	$("select").on("change", function(e) {
    	t.currencySelected = e.target.value;
    	var originalOptionInfo, converterOptionInfo;

    	$.each(t.parent.currencyData, function(i, item) {
            if (item.CharCode === t.parent.charCode) {
                originalOptionInfo = item;
            } else if (item.CharCode === t.currencySelected) {
                converterOptionInfo = item;
            };
            return (!(originalOptionInfo && converterOptionInfo));
        });

        function findNewPrice(price) {
        	return (originalOptionInfo.Value / originalOptionInfo.Nominal) * price / (converterOptionInfo.Value / converterOptionInfo.Nominal);
        };

    	$.each($(".delivery__item label"), function(i, item) {
        	var newDeliveryPrice = Math.round(findNewPrice($($(item).children()[0]).text()));
            $($(item).children()[0]).text(newDeliveryPrice);
            $($(item).children()[1]).text(converterOptionInfo.CharCode);
        });

        t.parent.price = Math.round(findNewPrice(t.parent.price));
        t.parent.charCode = t.currencySelected;

        if ($('input[name="deliveryId"]:checked').length) {
        	var checkedPrice = +$($($('input[name="deliveryId"]:checked').next()).children(".delivery__price")).text();
        	$(".order__price").text(t.parent.price + checkedPrice);
        } else {
        	$(".order__price").text(t.parent.price);
        };
        $(".order__price-charcode").text(t.parent.charCode);
    });
};

_delivery.prototype = {
	showValues: function(id) {
		var t = this, i, selectedItem;

        $(t.data).each(function(i) {
            if (t.data[i].id == id) {
                selectedItem = t.data[i];
                return false;
            }
        });

		$(".order__price").text(t.parent.price + selectedItem.price);

		if (selectedItem.needAdress) {
			t.deliveryAddressElement.show();
		} else {
			t.deliveryAddressElement.hide();
		};

		t.parent.payment.refresh(selectedItem.id);
	}
};

/*--------------------------------------------------------------------------------------------------------------------*/
/* Payment */
/*--------------------------------------------------------------------------------------------------------------------*/

_payment = function(parent, data) {
    var t = this, i;
    t.name = 'payment';
	t.parent = parent;
	t.data = data;
    t.element = $('.payment__items');

    $(t.data).each(function(i) {
        t.data[i].element = $("<div class='payment__item'></div>").appendTo(t.element);
		t.data[i].inputElement = $("<input class='payment__item__input' type='radio' name='paymentId' disabled id='" + t.data[i].id + "' value='" + t.data[i].id + "'/>").appendTo(t.data[i].element);
		t.data[i].labelElement = $("<label for=" + t.data[i].id + "> " + t.data[i].title + "</label>").appendTo(t.data[i].element);
    });
};

_payment.prototype = {
    refresh: function(id) {
		var t = this, i, ii, conj;
		$.get("https://netology-fbb-store-api.herokuapp.com/order/delivery/" + id + "/payment", function(deliveryPaymentData) {
			$(t.data).each(function(i) {
		        conj = false;
                $(deliveryPaymentData).each(function(ii) {
                    if (t.data[i].id == deliveryPaymentData[ii].id) {
                        conj = true;
                        return false;
                    };
                });
				if (conj) {
					t.data[i].element.show();
					t.data[i].inputElement.prop('disabled', false);
				} else {
					t.data[i].element.hide();
					t.data[i].inputElement.prop('disabled', true);
					t.data[i].inputElement.prop('checked', false);
				};
		    });
		});
    }
};

$(document).ready(function() {
    order = new _order();
});